import {user} from './user/user';
import {admin} from './admin/admin';

import './style.scss';

if (module.hot) {
    module.hot.accept();
}

console.log(user);
console.log(admin);

const root = document.createElement("div");
const buttonUser = document.createElement('button');
const buttonAdmin = document.createElement('button');

buttonAdmin.textContent = 'Add admin';
buttonUser.textContent = 'Add user';

buttonAdmin.onclick = () => admin();
buttonUser.onclick = () => user();

root.innerHTML = "my Webpack config";

document.body.appendChild(root);
root.appendChild(buttonUser);
root.appendChild(buttonAdmin);
