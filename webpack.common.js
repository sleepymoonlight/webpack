const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    entry: {
        main: './src/index.js',
        user: './src/user/user.js',
        admin: './src/admin/admin.js',
        common: './utilities/users.js'
    },
    output: {
        filename: '[name]/[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },
    optimization: {
        splitChunks: {
            chunks (chunk) {
                return chunk.name === 'common';
            }
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: 'html-es6-template-loader'
            },
            {
                test: /\.(js)$/,
                enforce: "pre",
                exclude: /node_modules/,
                use: ["babel-loader", "eslint-loader"]
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + "/index.html",
            inject: "body"
        }),
        new CleanWebpackPlugin()
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    }
};